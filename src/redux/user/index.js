import {USER_ACTIONS} from './action-types';
import moment from "moment";
import positions from "../../constants/positions";
import division from "../../constants/division";
import cuid from "cuid"

export const initialState = [
    {
        id:cuid(),
        secondName: 'Сумбаев',
        firstName: 'Павел',
        middleName: 'Андреевич',
        birthday: moment("1997-05-27").toDate(),
        serviceNumber: 1,
        position: positions.CTO,
        division: division.frontend
    },
    {
        id:cuid(),
        secondName: 'Кирилов',
        firstName: 'Олег',
        middleName: 'Александрович',
        birthday: moment("1997-05-27").toDate(),
        serviceNumber: 2,
        position: positions.CBDO,
        division: division.backend
    }
]

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_ACTIONS.SET_USER:
            return [
                ...state,
                {
                    id: cuid(),
                    secondName: action.data?.secondName,
                    firstName: action.data?.firstName,
                    middleName: action.data?.middleName,
                    birthday: action.data?.birthday,
                    serviceNumber: action.data?.serviceNumber,
                    position: action.data?.position,
                    division: action.data?.division,
                }
            ];
        case USER_ACTIONS.DELETE_USER:
            const id = state.findIndex((item, index, array)=>(action.id === item.id))
            const newArr = [...state]
            newArr.splice(id,1)
            console.log("newArr", newArr)
            return [
                ...newArr,
            ];
        case USER_ACTIONS.EDIT_USER:
            const editId = state.findIndex((item, index, array)=>(action.id === item.id))
            console.log("state", state)
            console.log("editId", editId)
            const editArr = [...state]
            console.log("editArr", editArr)
            editArr[editId] = {
                id: state[editId].id,
                secondName: action.data?.secondName,
                firstName: action.data?.firstName,
                middleName: action.data?.middleName,
                birthday: action.data?.birthday,
                serviceNumber: action.data?.serviceNumber,
                position: action.data?.position,
                division: action.data?.division,
            }
            console.log("editArrNew", editArr)
            return [
                ...editArr,
            ];
        default:
            return state
    }
}

export default UserReducer