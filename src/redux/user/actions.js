import {Dispatch} from 'redux';
import {USER_ACTIONS} from './action-types';

export const setUser = (data) => ({
    type: USER_ACTIONS.SET_USER,
    data
});

export const deleteUser = (id) => ({
    type: USER_ACTIONS.DELETE_USER,
    id
});

export const editUser = (id,data) => ({
    type: USER_ACTIONS.EDIT_USER,
    id,
    data
});
