export const USER_ACTIONS = {
    SET_USER:'user/SET_USER',
    EDIT_USER:'user/EDIT_USER',
    DELETE_USER:'user/DELETE_USER',
}
