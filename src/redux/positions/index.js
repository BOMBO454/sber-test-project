import {USER_ACTIONS} from './action-types';
import moment from "moment";
import positions from "../../constants/positions";

export const initialState = {
    users: [
        {secondName: 'Сумбаев', firstName: 'Павел', middleName: 'Андреевич', birthday: moment("1997-05-27").toDate(), serviceNumber:1,position:positions[CTO]}
    ]
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_ACTIONS.SET_USER:
            return {
                ...state,
            };
        default:
            return state
    }
}

export default UserReducer