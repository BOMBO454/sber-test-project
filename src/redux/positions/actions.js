import {Dispatch} from 'redux';
import {USER_ACTIONS} from './action-types';

export const setUser = (data) => (dispatch) => {
    dispatch({
        type: USER_ACTIONS.SET_USER,
        data
    });
};
