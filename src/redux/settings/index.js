import {SETTINGS_ACTIONS} from './action-types';

export const initialState = {
        secondName: true,
        firstName: false,
        middleName: false,
        birthday: false,
        serviceNumber: false,
        position: true,
        division: false
    }

const SettingReducer = (state = initialState, action) => {
    switch (action.type) {
        case SETTINGS_ACTIONS.SET_SETTING:
            return {
                ...state,
                ...action.data
                }
        default:
            return state
    }
}

export default SettingReducer