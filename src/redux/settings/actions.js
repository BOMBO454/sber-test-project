import {Dispatch} from 'redux';
import {SETTINGS_ACTIONS} from './action-types';

export const setSetting = (data) => ({
    type: SETTINGS_ACTIONS.SET_SETTING,
    data
});
