import {combineReducers} from 'redux'
import UserReducer from "./user/index";
import SettingReducer from "./settings/index";

export const rootReducer = combineReducers({
    users: UserReducer,
    settings: SettingReducer
})