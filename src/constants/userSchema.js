import * as yup from "yup";
import moment from "moment";

export const validationSchema = yup.object().shape({
    secondName: yup.string().required('Обязательно для заполнения').min(2, 'Минимально 2 символа').max(32, 'максимально 32 символа'),
    firstName: yup.string().required('Обязательно для заполнения').min(2, 'Минимально 2 символа').max(32, 'максимально 32 символа'),
    middleName: yup.string().min(2, 'Минимально 2 символа').max(32, 'максимально 32 символа'),
    birthday: yup.date().required('Обязательно для заполнения').max(moment().toDate(), 'не может быть будущим'),
    serviceNumber: yup.number().required('Обязательно для заполнения'),
    position: yup.string().required('Обязательно для заполнения'),
    division: yup.string().required('Обязательно для заполнения'),
});