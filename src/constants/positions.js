export default {
    CFO: "chief financial officer",
    CTO: "chief technology officer",
    CCO: "chief commercial officer",
    CBDO: "chief business development officer",
    CBO: "chief business officer",
    CIO: "chief information officer",
    CPO: "chief product officer",
    CMO: "chief marketing officer",
}