import React, {useContext} from 'react';
import S from './Header.module.scss'
import {Button, IconButton, Toolbar, Typography, AppBar} from '@material-ui/core'
import {Menu} from '@material-ui/icons'
import {SideBarContext} from "../Context/SideBarContext";

type Props = {}

export default function Header({}: Props) {
    const {openBar, toggleBar} = useContext(SideBarContext)
    return (
        <AppBar className={S.Header} position="relative">
            <Toolbar>
                <IconButton onClick={toggleBar} edge="start" className={S.menuButton} color="inherit" aria-label="menu">
                    <Menu/>
                </IconButton>
                <Typography variant="h6" className={S.title}>
                    Система ведения сотрудников
                </Typography>
            </Toolbar>
        </AppBar>
    );
}
