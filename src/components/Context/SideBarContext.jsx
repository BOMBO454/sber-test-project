import React, {useState} from 'react'

export const SideBarContext = React.createContext()


export const SideBarProvider = ({children}) => {
    const [openBar, setOpenBar] = useState(false)
    const toggleBar = () => {
        setOpenBar(!openBar)
    }
    return (
        <SideBarContext.Provider value={{openBar, toggleBar}}>
            {children}
        </SideBarContext.Provider>
    )
}