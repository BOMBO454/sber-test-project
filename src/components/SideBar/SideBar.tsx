import React, {useContext} from 'react';
import { useHistory } from "react-router-dom";
import {Divider, Drawer, List, ListItem, ListItemIcon, ListItemText, Toolbar } from '@material-ui/core';
import { List as ListIcon, Add as AddIcon, Settings as SettingsIcon } from '@material-ui/icons';
import S from './SideBar.module.scss'
import routes from "../../constants/routes";
import {SideBarContext} from "../Context/SideBarContext";

type Props = {}

export default function SideBar({}: Props) {
    const history = useHistory();
    const {openBar, toggleBar} = useContext(SideBarContext)
    return (
        <Drawer
            className={S.SideBar}
            open={openBar}
            variant="persistent"
        >
            <Toolbar />
            <div className={S.drawerContainer}>
                <List>
                    <ListItem button onClick={()=>{history.push(routes.main)}}>
                        <ListItemIcon><ListIcon /></ListItemIcon>
                        <ListItemText primary="Список сотрудников" />
                    </ListItem>
                    <ListItem button onClick={()=>{history.push(routes.addUser)}}>
                        <ListItemIcon><AddIcon /></ListItemIcon>
                        <ListItemText primary="Добавить запись" />
                    </ListItem>
                    <ListItem button onClick={()=>{history.push(routes.settings)}}>
                        <ListItemIcon><SettingsIcon /></ListItemIcon>
                        <ListItemText primary="Настройки" />
                    </ListItem>
                </List>
            </div>
        </Drawer>
    );
}
