import React from "react";
import ReactDOM from 'react-dom';
import 'normalize.css'
import App from './App';
import reportWebVitals from './reportWebVitals';
import {compose, createStore} from "redux";
import {Provider} from 'react-redux'
import {rootReducer} from "./redux/rootReducer";

// @ts-ignore
const store = createStore(rootReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
