import React from 'react';
import {
    Box,
    Button,
    Container,
    FormControl, Grid,
    InputLabel,
    List,
    ListItem,
    MenuItem,
    Select,
    TextField
} from "@material-ui/core";
import {setUser} from '../../redux/user/actions'
import {connect} from 'react-redux'
import * as yup from 'yup';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from "moment";
import S from "../UserList/UserList.module.scss";
import {Form, Formik} from 'formik';
import {FormHelperText} from '@material-ui/core';
import {Paper} from '@material-ui/core';
import {LinearProgress} from '@material-ui/core';
import {useHistory} from "react-router-dom";
import routes from "../../constants/routes";
import { validationSchema } from '../../constants/userSchema';
import positions from "../../constants/positions";
import division from "../../constants/division";

type Props = {}

const UserAdd = ({setUser}: any) => {
    console.log("props", setUser)
    const history = useHistory();
    return (
        <Container className={S.UserList}>
            <Formik
                initialValues={{
                    secondName: "",
                    firstName: "",
                    middleName: "",
                    birthday: null,
                    serviceNumber: "",
                    position: null,
                    division: null,
                    continue: false,
                }}
                validationSchema={validationSchema}
                onSubmit={(values, {setSubmitting, resetForm,...props}) => {
                    setTimeout(()=>{
                        setSubmitting(true)
                        setUser(values)
                        if(values.continue === false){
                            history.push(routes.main)
                        }else{
                            resetForm()
                        }
                    },1000)
                    setSubmitting(false)
                }}
                validateOnChange={false}
                validateOnBlur={false}
            >
                {({
                      values,
                      errors,
                      handleChange,
                      handleBlur,
                      setFieldValue,
                      isSubmitting,
                      submitForm,
                      resetForm,
                      handleSubmit,
                      handleReset
                  }) => (
                    <Box m={4}>
                            <Paper elevation={3}>
                                {isSubmitting && <LinearProgress/>}
                                <List>
                                    <ListItem>
                                        <TextField defaultValue={undefined} onReset={(e)=>{
                                            console.log("reset", e)}} onBlur={handleBlur} error={!!errors.secondName}
                                                   helperText={errors.secondName}  onChange={(e) => {
                                            setFieldValue("secondName", e.target.value)
                                        }}
                                                   name={"secondName"}
                                                   value={values.secondName} fullWidth label="Фамилия"/>
                                    </ListItem>
                                    <ListItem>
                                        <TextField onBlur={handleBlur} error={!!errors.firstName}
                                                   helperText={errors.firstName}
                                                    onChange={handleChange} name={"firstName"}
                                                   value={values.firstName} fullWidth label="Имя"/>
                                    </ListItem>
                                    <ListItem>
                                        <TextField onBlur={handleBlur} error={!!errors.middleName}
                                                   helperText={errors.middleName} value={values.middleName}
                                                   onChange={handleChange}
                                                   name={"middleName"} fullWidth label="Отчество"/>
                                    </ListItem>
                                    <ListItem>
                                        <MuiPickersUtilsProvider utils={MomentUtils}>
                                            <KeyboardDatePicker
                                                onBlur={handleBlur}
                                                error={!!errors.birthday}
                                                helperText={errors.birthday}

                                                fullWidth
                                                disableToolbar
                                                variant="inline"
                                                format="DD/MM/yyyy"
                                                margin="normal"
                                                id="date-picker-inline"
                                                label="Дата рождения"
                                                value={values.birthday}
                                                initialFocusedDate={undefined}
                                                name={"birthday"}
                                                onChange={(e) => {
                                                    if (e?.isValid()) {
                                                        setFieldValue("birthday", e?.toDate())
                                                    }
                                                }}
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change date',
                                                }}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </ListItem>
                                    <ListItem>
                                        <TextField
                                            onBlur={handleBlur}
                                            error={!!errors.serviceNumber}
                                            helperText={errors.serviceNumber}
                                             value={values.serviceNumber} onChange={handleChange}
                                            name={"serviceNumber"} fullWidth type="Number" label="Табельный номер"/>
                                    </ListItem>
                                    <ListItem>
                                        <FormControl fullWidth>
                                            <InputLabel error={!!errors.position}
                                                        htmlFor="id-position">Должность</InputLabel>
                                            <Select
                                                error={!!errors.position}
                                                onBlur={handleBlur}
                                                name={"position"} value={values.position}
                                                inputProps={{id: "id-position"}} fullWidth label="Должность"
                                                onChange={e => {
                                                    setFieldValue("position", e.target.value)
                                                }}>
                                                <MenuItem value={undefined}>
                                                    <em>-</em>
                                                </MenuItem>
                                                {Object.values(positions).map(_positions=>{
                                                    return(
                                                        <MenuItem value={_positions}>{_positions}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                            {errors.position &&
                                            <FormHelperText error>{errors.position}</FormHelperText>}
                                        </FormControl>
                                    </ListItem>
                                    <ListItem>
                                        <FormControl  fullWidth>
                                            <InputLabel error={!!errors.division}
                                                        htmlFor="id-division">Подразделение</InputLabel>
                                            <Select
                                                onBlur={handleBlur}
                                                error={!!errors.position}
                                                value={values.division}
                                                inputProps={{id: "id-division"}}
                                                fullWidth
                                                label="Подразделение" name="division"
                                                onChange={e => {
                                                    setFieldValue("division", e.target.value)
                                                }}>
                                                <MenuItem value={undefined}>
                                                    <em>-</em>
                                                </MenuItem>
                                                {Object.values(division).map(_division=>{
                                                    return(
                                                        <MenuItem value={_division}>{_division}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                            {errors.division &&
                                            <FormHelperText error>{errors.division}</FormHelperText>}
                                        </FormControl>
                                    </ListItem>
                                    <ListItem>
                                        <Box mr={2}>
                                            <Button onClick={(e) => {
                                                setFieldValue('continue', true, false)
                                                submitForm()
                                            }} disabled={isSubmitting} type="submit" color={"primary"}
                                                    variant={"contained"}>
                                                Сохранить и добавить еще
                                            </Button>
                                        </Box>
                                        <Box>
                                            <Button onClick={(e)=>{
                                                setFieldValue('continue', false, false)
                                                submitForm()
                                            }} disabled={isSubmitting} type="submit"
                                                    color={"secondary"}
                                                    variant={"contained"}>Сохранить и
                                                вернуться в
                                                список
                                            </Button>
                                        </Box>
                                    </ListItem>
                                </List>
                            </Paper>
                        </Box>
                )}
            </Formik>
        </Container>
    );
}

const mapStateToProps = (state: any) => {
    return {users: state.users}
}

const mapDispatchToProps = {
    setUser
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAdd)