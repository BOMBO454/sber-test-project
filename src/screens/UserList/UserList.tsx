import React, {useState} from 'react';
import S from './UserList.module.scss'
import {connect} from 'react-redux'
import {
    Container,
    Paper,
    Table,
    TableBody,
    TableContainer,
    TableRow,
    TableHead,
    TableCell,
    Box,
    Drawer,
    TextField,
    Toolbar,
    List,
    ListItem,
    MenuItem, FormControl, Button, FormHelperText
} from '@material-ui/core';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import {Select} from '@material-ui/core';
import {InputLabel} from '@material-ui/core';
import moment from 'moment';
import positions from "../../constants/positions";
import division from "../../constants/division";
import {useFormik} from "formik";
import {validationSchema} from '../../constants/userSchema';
import {deleteUser, editUser} from "../../redux/user/actions";

const Userlist = ({users, settings, deleteUser, editUser}: any) => {
    const [showDriver, setShowDriver] = useState(false)
    const [isEditable, setIsEditable] = useState(false)
    const [driverData, setDriverData] = useState({
        secondName: "",
        firstName: "",
        middleName: "",
        birthday: null,
        serviceNumber: "",
        position: null,
        division: null,
        continue: false,
        id: undefined,
        tempId: undefined,
    })

    const {
        values,
        errors,
        handleChange,
        handleBlur,
        setFieldValue,
        isSubmitting,
        submitForm,
        setValues,
    } = useFormik({
        initialValues: driverData,
        validationSchema: validationSchema,
        onSubmit: (values, {setSubmitting, resetForm, ...props}) => {
            editUser(values.id, values)
            setSubmitting(false)
        },
        validateOnChange: false,
        validateOnBlur: false,
    });
    const userSelectHandler = (id: number) => {
        const data = users.find((user: any) => (user.serviceNumber === id))
        if (data) {
            setShowDriver(true)
            setDriverData(data)
            setValues(data)
        }
    }
    return (
        <Container className={S.UserList}>
            <Box m={4}>
                <Paper elevation={3}>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    {settings.secondName && <TableCell>Фамилия</TableCell>}
                                    {settings.firstName && <TableCell>Имя</TableCell>}
                                    {settings.middleName && <TableCell>Отчество</TableCell>}
                                    {settings.birthday && <TableCell>Дата рождения</TableCell>}
                                    {settings.serviceNumber && <TableCell>Табельный номер</TableCell>}
                                    {settings.position && <TableCell>Должность</TableCell>}
                                    {settings.division && <TableCell>Подразделение</TableCell>}
                                    <TableCell align={"right"}>Подробнее</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {users.map((user: any) => (
                                    <TableRow key={user.id}>
                                        {settings.secondName && <TableCell>{user.secondName}</TableCell>}
                                        {settings.firstName && <TableCell>{user.firstName}</TableCell>}
                                        {settings.middleName && <TableCell>{user.middleName}</TableCell>}
                                        {settings.birthday &&
                                        <TableCell>{moment(user.birthday).format('DD/MM/yyyy')}</TableCell>}
                                        {settings.serviceNumber && <TableCell>{user.serviceNumber}</TableCell>}
                                        {settings.position && <TableCell>{user.position}</TableCell>}
                                        {settings.division && <TableCell>{user.division}</TableCell>}
                                        <TableCell align={"right"}><Button onClick={() => {
                                            userSelectHandler(user.serviceNumber)
                                        }} variant={"contained"} color={'primary'}>Подробнее</Button></TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>
            </Box>
            <Drawer
                className={S.Drawer}
                variant="persistent"
                anchor="right"
                open={showDriver}
            >
                <Toolbar/>
                <List>
                    <ListItem><Button onClick={() => {
                        setShowDriver(false)
                        setIsEditable(false)
                    }} color={"secondary"} variant={"contained"}>Скрыть</Button></ListItem>
                    <ListItem>
                        <TextField disabled={!isEditable} defaultValue={undefined} onReset={(e) => {
                            console.log("reset", e)
                        }} onBlur={handleBlur} error={!!errors.secondName}
                                   helperText={errors.secondName} onChange={(e) => {
                            setFieldValue("secondName", e.target.value)
                        }}
                                   name={"secondName"}
                                   value={values.secondName} fullWidth label="Фамилия"/>
                    </ListItem>
                    <ListItem>
                        <TextField disabled={!isEditable} onBlur={handleBlur} error={!!errors.firstName}
                                   helperText={errors.firstName}
                                   onChange={handleChange} name={"firstName"}
                                   value={values.firstName} fullWidth label="Имя"/>
                    </ListItem>
                    <ListItem>
                        <TextField disabled={!isEditable} onBlur={handleBlur} error={!!errors.middleName}
                                   helperText={errors.middleName} value={values.middleName}
                                   onChange={handleChange}
                                   name={"middleName"} fullWidth label="Отчество"/>
                    </ListItem>
                    <ListItem>
                        <MuiPickersUtilsProvider utils={MomentUtils}>
                            <KeyboardDatePicker
                                disabled={!isEditable}
                                onBlur={handleBlur}
                                error={!!errors.birthday}
                                helperText={errors.birthday}

                                fullWidth
                                disableToolbar
                                variant="inline"
                                format="DD/MM/yyyy"
                                margin="normal"
                                id="date-picker-inline"
                                label="Дата рождения"
                                value={values.birthday}
                                initialFocusedDate={undefined}
                                name={"birthday"}
                                onChange={(e) => {
                                    if (e?.isValid()) {
                                        setFieldValue("birthday", e?.toDate())
                                    }
                                }}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </ListItem>
                    <ListItem>
                        <TextField
                            disabled={!isEditable}
                            onBlur={handleBlur}
                            error={!!errors.serviceNumber}
                            helperText={errors.serviceNumber}
                            value={values.serviceNumber} onChange={handleChange}
                            name={"serviceNumber"} fullWidth type="Number" label="Табельный номер"/>
                    </ListItem>
                    <ListItem>
                        <FormControl disabled={!isEditable} fullWidth>
                            <InputLabel disabled={!isEditable} error={!!errors.position}
                                        htmlFor="id-position">Должность</InputLabel>
                            <Select
                                disabled={!isEditable}
                                error={!!errors.position}
                                onBlur={handleBlur}
                                name={"position"} value={values.position}
                                inputProps={{id: "id-position"}} fullWidth label="Должность"
                                onChange={e => {
                                    setFieldValue("position", e.target.value)
                                }}>
                                <MenuItem value={undefined}>
                                    <em>-</em>
                                </MenuItem>
                                {Object.values(positions).map(_positions => {
                                    return (
                                        <MenuItem value={_positions}>{_positions}</MenuItem>
                                    )
                                })}
                            </Select>
                            {errors.position &&
                            <FormHelperText error>{errors.position}</FormHelperText>}
                        </FormControl>
                    </ListItem>
                    <ListItem>
                        <FormControl disabled={!isEditable} fullWidth>
                            <InputLabel disabled={!isEditable} error={!!errors.division}
                                        htmlFor="id-division">Подразделение</InputLabel>
                            <Select
                                disabled={!isEditable}
                                onBlur={handleBlur}
                                error={!!errors.position}
                                value={values.division}
                                inputProps={{id: "id-division"}}
                                fullWidth
                                label="Подразделение" name="division"
                                onChange={e => {
                                    setFieldValue("division", e.target.value)
                                }}>
                                <MenuItem value={undefined}>
                                    <em>-</em>
                                </MenuItem>
                                {Object.values(division).map(_division => {
                                    return (
                                        <MenuItem value={_division}>{_division}</MenuItem>
                                    )
                                })}
                            </Select>
                            {errors.division &&
                            <FormHelperText error>{errors.division}</FormHelperText>}
                        </FormControl>
                    </ListItem>
                    <ListItem>
                        {isEditable ? <><Box mr={2}>
                            <Button onClick={(e) => {
                                setFieldValue('id', values.id, false)
                                submitForm()
                            }} disabled={isSubmitting} type="submit" color={"primary"}
                                    variant={"contained"}>
                                Сохранить
                            </Button>
                        </Box>
                            <Box>
                                <Button onClick={() => {
                                    setValues(driverData)
                                    setIsEditable(false)
                                }} disabled={isSubmitting} type="submit"
                                        color={"secondary"}
                                        variant={"contained"}>Отменить
                                </Button>
                            </Box></> : <>
                            <Box mr={2}>
                                <Button onClick={() => {
                                    setIsEditable(true)
                                }} type="submit" color={"primary"}
                                        variant={"contained"}>
                                    Редактировать
                                </Button>
                            </Box>
                            <Box mr={2}>
                                <Button onClick={(e) => {
                                    deleteUser(values.id)
                                    setShowDriver(false)
                                }} disabled={isSubmitting} type="submit" color={"secondary"}
                                        variant={"contained"}>
                                    Удалить
                                </Button>
                            </Box>
                        </>}
                    </ListItem>
                </List>
            </Drawer>
        </Container>
    );
}

const mapStateToProps = (state: any) => {
    return {
        users: state.users,
        settings: state.settings
    }
}

const mapDispatchToProps = {
    deleteUser,
    editUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Userlist)