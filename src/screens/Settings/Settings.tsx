import React from 'react';
import S from './Settings.module.scss'
import {connect} from 'react-redux'
import {
    Container,
    Paper,
    Box,
    List,
    ListItem, Button, FormControlLabel, Checkbox
} from '@material-ui/core';
import {setSetting} from '../../redux/settings/actions'
import {Formik} from "formik";
import {Grid} from '@material-ui/core';

const Settings = ({settings, setSetting}: any) => {
    return (
        <Container className={S.UserList}>
            <Box m={4}>
                <Paper elevation={3}>
                    <Formik
                        initialValues={{
                            secondName: settings.secondName,
                            firstName: settings.firstName,
                            middleName: settings.middleName,
                            birthday: settings.birthday,
                            serviceNumber: settings.serviceNumber,
                            position: settings.position,
                            division: settings.division,
                            continue: settings.continue,
                        }}
                        onSubmit={(values) => {
                            setSetting(values)
                        }}
                        validateOnChange={false}
                        validateOnBlur={false}
                    >
                        {({
                              values,
                              errors,
                              handleChange,
                              handleBlur,
                              setFieldValue,
                              isSubmitting,
                              submitForm,
                              resetForm,
                              handleSubmit,
                              handleReset
                          }) => (
                            <List>

                                <ListItem><FormControlLabel
                                    control={<Checkbox checked={values.secondName} name={"secondName"}
                                                       onChange={handleChange}/>}
                                    label="secondName"
                                />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.firstName} name={"firstName"}
                                                           onChange={handleChange}/>}
                                        label="firstName"
                                    />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.middleName} name={"middleName"}
                                                           onChange={handleChange}/>}
                                        label="middleName"
                                    />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.birthday} name={"birthday"}
                                                           onChange={handleChange}/>}
                                        label="birthday"
                                    />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.serviceNumber} name={"serviceNumber"}
                                                           onChange={handleChange}/>}
                                        label="serviceNumber"
                                    />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.position} name={"position"}
                                                           onChange={handleChange}/>}
                                        label="position"
                                    />
                                </ListItem>
                                <ListItem>
                                    <FormControlLabel
                                        control={<Checkbox checked={values.division} name={"division"}
                                                           onChange={handleChange}/>}
                                        label="division"
                                    />
                                </ListItem>
                                <ListItem>
                                    <Grid container spacing={2}>
                                        <Grid item>
                                            <Button onClick={() => {
                                                submitForm()
                                            }} type="submit"
                                                    color={"primary"}
                                                    variant={"contained"}>Применить
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button onClick={() => {
                                                resetForm()
                                            }} type="submit"
                                                    color={"secondary"}
                                                    variant={"outlined"}>Сбросить
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                            </List>
                        )}
                    </Formik>
                </Paper>
            </Box>
        </Container>
    );
}

const mapStateToProps = (state: any) => {
    return {settings: state.settings}
}

const mapDispatchToProps = {
    setSetting
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)