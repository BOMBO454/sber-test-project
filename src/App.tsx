import React, {useState} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Header from './components/Header/Header';
import SideBar from "./components/SideBar/SideBar";
import UserList from "./screens/UserList/UserList";
import UserAdd from './screens/UserAdd/UserAdd';
import paths from "./constants/routes"
import {SideBarProvider} from "./components/Context/SideBarContext";
import Settings from "./screens/Settings/Settings";

function App() {
    return (
        <div className="App">
            <Router>
                <SideBarProvider >
                    <Header/>
                    <SideBar/>
                </SideBarProvider>
                <Switch>
                    <Route exact path={paths.main}>
                        <UserList/>
                    </Route>
                    <Route exact path={paths.addUser}>
                        <UserAdd/>
                    </Route>
                    <Route exact path={paths.settings}>
                        <Settings/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
